# vcloud-ubuntu
A small sample to bring up two ubuntu1204 machines in vCloud with vagrant-vcloud provider
and Vagrant 1.6.2.

## Prerequisites

* Vagrant 1.6.2

## Create the boxes
```bash
vagrant up --provider=vcloud
```

